﻿/*
 *  Thermite Toolkit
 *  Copyright (C) 2016 Thermite Gorilla
 *
 *  Details about the project, including developer guides, can be found at:
 *     http://thermite-gorilla.co.uk/projects/thermitetoolkit
 *
 *  This program is provided to you under the terms of the Microsoft Public License.
 *  Details of the license are published at:
 *     http://thermite-gorilla.co.uk/projects/thermitetoolkit/license 
 */

namespace Thermite.Controls
{
   /// <summary>
   /// Class defining constants for visual states of controls.
   /// </summary>
   internal static partial class VisualStates
   {
   }
}
