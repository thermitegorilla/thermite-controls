﻿/*
 *  Thermite Toolkit
 *  Copyright (C) 2016 Thermite Gorilla
 *
 *  Details about the project, including developer guides, can be found at:
 *     http://thermite-gorilla.co.uk/projects/thermitetoolkit
 *
 *  This program is provided to you under the terms of the Microsoft Public License.
 *  Details of the license are published at:
 *     http://thermite-gorilla.co.uk/projects/thermitetoolkit/license 
 */

namespace Thermite.Controls
{
   using System.Windows;
   using System.Windows.Controls;
   using System.Windows.Media;

   /// <summary>
   /// Class representing the <see cref="ContentOverlay"/> custom control.
   /// </summary>
   [TemplateVisualState(Name = VisualStates.StateContentOverlayHidden, GroupName = VisualStates.GroupContentOverlayStatus)]
   [TemplateVisualState(Name = VisualStates.StateContentOverlayVisible, GroupName = VisualStates.GroupContentOverlayStatus)]
   public class ContentOverlay : ContentControl
   {
      /// <summary>
      /// Static constructor for the <see cref="ContentOverlay"/> class.
      /// </summary>
      static ContentOverlay()
      {
         DefaultStyleKeyProperty.OverrideMetadata(typeof(ContentOverlay), new FrameworkPropertyMetadata(typeof(ContentOverlay)));
      }

      #region Overlay
      
      /// <summary>
      /// Dependency property for the <see cref="Overlay"/> property.
      /// </summary>
      public static readonly DependencyProperty OverlayProperty = DependencyProperty.Register(nameof(Overlay), typeof(object), typeof(ContentOverlay),
         new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.None));
      
      /// <summary>
      /// Gets and sets the <see cref="Overlay"/> content.
      /// </summary>
      public object Overlay
      {
         get { return GetValue(OverlayProperty); }
         set { SetValue(OverlayProperty, value); }
      }

      #endregion

      #region IsOverlayVisible

      /// <summary>
      /// Dependency property for the <see cref="IsOverlayVisible"/> property.
      /// </summary>
      public static readonly DependencyProperty IsOverlayVisibleProperty = DependencyProperty.Register(nameof(IsOverlayVisible), typeof(bool), typeof(ContentOverlay),
         new PropertyMetadata(false, new PropertyChangedCallback(IsOverlayVisibleChanged)));

      /// <summary>
      /// Gets and sets whether or not the overlay should be visible.
      /// </summary>
      public bool IsOverlayVisible
      {
         get { return (bool)GetValue(IsOverlayVisibleProperty); }
         set { SetValue(IsOverlayVisibleProperty, value); }
      }

      /// <summary>
      /// Static property change listener for the <see cref="IsOverlayVisible"/> property.
      /// </summary>
      /// <param name="d"></param>
      /// <param name="e"></param>
      public static void IsOverlayVisibleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) =>
         ((ContentOverlay)d).IsOverlayVisibleChanged(e);

      /// <summary>
      /// Instance property change listener for the <see cref="IsOverlayVisible"/> property.
      /// </summary>
      /// <param name="e"></param>
      protected virtual void IsOverlayVisibleChanged(DependencyPropertyChangedEventArgs e) =>
         ChangeVisualState(true);

      #endregion

      #region GlassPaneBackground

      /// <summary>
      /// Dependency property for the <see cref="GlassPaneBackground"/> property.
      /// </summary>
      public static readonly DependencyProperty GlassPaneBackgroundProperty = DependencyProperty.Register(nameof(GlassPaneBackground), typeof(Brush), typeof(ContentOverlay),
         new FrameworkPropertyMetadata(new SolidColorBrush(Colors.Transparent), FrameworkPropertyMetadataOptions.None));

      /// <summary>
      /// Gets and sets the <see cref="Brush"/> to use for the <see cref="GlassPaneBackground"/>.
      /// </summary>
      public Brush GlassPaneBackground
      {
         get { return (Brush)GetValue(GlassPaneBackgroundProperty); }
         set { SetValue(GlassPaneBackgroundProperty, value); }
      }

      #endregion

      #region GlassPaneOpacity

      /// <summary>
      /// Dependency property for the <see cref="GlassPaneOpacity"/> property.
      /// </summary>
      public static readonly DependencyProperty GlassPaneOpacityProperty = DependencyProperty.Register(nameof(GlassPaneOpacity), typeof(double), typeof(ContentOverlay),
         new FrameworkPropertyMetadata(default(double), FrameworkPropertyMetadataOptions.None));

      /// <summary>
      /// Gets and sets the opacity value for the glass pane.
      /// </summary>
      public double GlassPaneOpacity
      {
         get { return (double)GetValue(GlassPaneOpacityProperty); }
         set { SetValue(GlassPaneOpacityProperty, value); }
      }

      #endregion

      #region GlassPaneOpacityMask

      /// <summary>
      /// Dependency property for the <see cref="GlassPaneOpacityMask"/> property.
      /// </summary>
      public static readonly DependencyProperty GlassPaneOpacityMaskProperty = DependencyProperty.Register(nameof(GlassPaneOpacityMask), typeof(Brush), typeof(ContentOverlay),
         new FrameworkPropertyMetadata(new SolidColorBrush(Colors.Black), FrameworkPropertyMetadataOptions.None));

      /// <summary>
      /// Gets and sets the <see cref="Brush"/> to use for the <see cref="GlassPaneOpacityMask"/>.
      /// </summary>
      public Brush GlassPaneOpacityMask
      {
         get { return (Brush)GetValue(GlassPaneOpacityMaskProperty); }
         set { SetValue(GlassPaneOpacityMaskProperty, value); }
      }

      #endregion

      #region Methods

      /// <summary>
      /// Overrides the OnApplyTemplate method.
      /// </summary>
      public override void OnApplyTemplate()
      {
         base.OnApplyTemplate();
         ChangeVisualState(false);
      }

      /// <summary>
      /// Change the control's visual state.
      /// </summary>
      /// <param name="useTransitions">True if visual transitions should be used.</param>
      protected virtual void ChangeVisualState(bool useTransitions)
      {
         VisualStateManager.GoToState(this, IsOverlayVisible ? VisualStates.StateContentOverlayVisible : VisualStates.StateContentOverlayHidden, useTransitions);
      }

      #endregion
   }
}
