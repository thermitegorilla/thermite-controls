﻿/*
 *  Thermite Toolkit
 *  Copyright (C) 2016 Thermite Gorilla
 *
 *  Details about the project, including developer guides, can be found at:
 *     http://thermite-gorilla.co.uk/projects/thermitetoolkit
 *
 *  This program is provided to you under the terms of the Microsoft Public License.
 *  Details of the license are published at:
 *     http://thermite-gorilla.co.uk/projects/thermitetoolkit/license 
 */

namespace Thermite.Controls
{
   /// <summary>
   /// Visual states for the <see cref="ContentOverlay"/>.
   /// </summary>
   internal static partial class VisualStates
   {
      /// <summary>
      /// Group name for <see cref="ContentOverlay"/> states.
      /// </summary>
      public const string GroupContentOverlayStatus = "ContentOverlayStates";

      /// <summary>
      /// Visible state for <see cref="ContentOverlay"/>.
      /// </summary>
      public const string StateContentOverlayVisible = "Visible";

      /// <summary>
      /// Hidden state for <see cref="ContentOverlay"/>.
      /// </summary>
      public const string StateContentOverlayHidden = "Hidden";
   }
}
